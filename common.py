#!/usr/bin/env python
# -*- coding: UTF-8 -*-
'''
@Project ：trade
@File    ：common.py.py
@Author  ：wangzheng
@Date    ：2022/03/22 11:49 
'''

# common.py
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

class DBComm:
    def __init__(self):
        self.engine = self.connect_to_db()
        Session = sessionmaker(bind=self.engine)
        self.session = Session()

    def connect_to_db(self):
        database_url = "postgresql://user:123456@127.0.0.1:5432/idc"
        try:
            engine = create_engine(database_url)
            return engine
        except Exception as e:
            print(f"连接数据库失败: {e}")

