from common import DBComm
import pandas as pd
from common import DBComm
from sqlalchemy.sql import text  # 导入 text 函数


def save_table_pandas(table_name, df, index=False, if_exists='append'):
    comm = DBComm()
    try:
        df.to_sql(name=table_name, con=comm.engine, index=index, if_exists=if_exists)
        print(f"数据成功保存到 {table_name}")
    except Exception as e:
        print(f"保存数据失败，错误信息：{e}")


def load_table_pandas(sql):
    comm = DBComm()
    try:
        # 使用 text() 包装 SQL 语句
        result_proxy = comm.session.execute(text(sql))
        
        # 将结果转换为 DataFrame
        df = pd.DataFrame(result_proxy.fetchall())
        df.columns = result_proxy.keys()
        
        return df
    except Exception as e:
        print(f"加载数据失败，错误信息：{e}")
        return None
