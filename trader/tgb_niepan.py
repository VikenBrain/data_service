#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
@File    :   np.py
@Time    :   2023/09/02 09:31:40
@Author  :   viken 
@Email   :   vikenbrain@gmail.com 
@Site    :   
@Version :   1.0
@Desc    :   抓取涅槃重生的交割单
'''

import os
import requests
from lxml import etree

if not os.path.exists("photo"):
    os.makedirs("photo")


url = "https://www.taoguba.com.cn/Article/1477629/2"
headers = {
    "cookie":"gdp_user_id=gioenc-bd260a79%2Cgeg2%2C5387%2C949g%2C9g7cb51598c6; Actionshow2=true; 893eedf422617c96_gdp_sequence_ids=%7B%22globalKey%22%3A6%2C%22PAGE%22%3A6%7D; JSESSIONID=YWQzYmIxOWUtNmI3Ni00ZWFmLThkMTYtN2E2M2M0MGVlYWZm; Hm_lvt_cc6a63a887a7d811c92b7cc41c441837=1691043819,1691691608,1692070861,1693604725; agree=enter; tgbuser=7730686; tgbpwd=62dfa218a717526f9dcd58650ddb434e30d7577297d0353a4f001b8cbd0a1bcc47e7w9vtu5ts2th; 893eedf422617c96_gdp_gio_id=gioenc-6621797; 893eedf422617c96_gdp_cs1=gioenc-6621797; creatorStatus7730686=true; loginStatus=account; 893eedf422617c96_gdp_session_id=15d88c22-2a1b-452a-9066-da0fde9fb491; 893eedf422617c96_gdp_sequence_ids=%7B%22globalKey%22%3A227%2C%22PAGE%22%3A142%2C%22CUSTOM%22%3A81%2C%22VISIT%22%3A6%7D; 893eedf422617c96_gdp_session_id_15d88c22-2a1b-452a-9066-da0fde9fb491=true; Hm_lpvt_cc6a63a887a7d811c92b7cc41c441837=1693618045",
    "user-agent":"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36"
}

resp = requests.get(url, headers=headers)
# with open("output.html", "w") as file:
#     file.write(resp.text)

# Parse the HTML content
html = etree.HTML(resp.content)

# Use XPath to extract data
data = html.xpath('//div[@class="comment-data-text"]/div[2]/img/@data-original')

# Print the extracted data
for img_url in data:
    img_resp = requests.get(img_url)
    img_name = img_url.split("/")[-1]  # Extract the image name from the URL
    img_path = os.path.join("photo", img_name)  # Create the full path to save the image
    with open(img_path, "wb") as img_file:
        img_file.write(img_resp.content)
        print(f"Downloaded image: {img_name}")





# "//div[@class="comment-data-text"]/div[2]"

