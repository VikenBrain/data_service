import akshare as ak
import pandas
import mplfinance as mpf


def get_hangqing(symbo: str):
    """
    获取某个历史合约的行情
    :param symbo:
    :return:
    """
    result: pandas.DataFrame = ak.futures_zh_daily_sina(symbol=symbo)
    result.set_index("date", inplace=True)
    result.index = pandas.to_datetime(result.index)
    return result


def to_excel(name: str):
    """
    df保存为excel
    :param name:
    :return:
    """
    a: pandas.DataFrame = get_hangqing(name)
    a.to_excel(f"{name}.xlsx")


def show_candle(symbo: str):
    """
    绘制candle
    :param symbo:
    :return:
    """
    df = get_hangqing(symbo=symbo)
    mpf.plot(df, type='candle', volume=True)


show_candle("m2309")
